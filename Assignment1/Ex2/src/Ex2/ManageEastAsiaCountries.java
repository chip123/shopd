package Ex2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ManageEastAsiaCountries {
	static List<EastAsiaCountries> countries = new ArrayList<EastAsiaCountries>();

	public static void main(String[] args) throws Exception {
		int key =0;
		while(key!=5) {
			System.out.println();
		System.out.println("1. Nhập thông tin cho 11 đất nước ở Đông Nam Á.\r\n" + "2. Hiển thị thông tin vừa nhập\r\n"
				+ "3. Tìm kiếm thông tin đất nước theo tên nước nhập vào.\r\n"
				+ "4. Hiển thị thông tin tăng dần theo tên đất nước.\r\n" + "5. Thoát");
		Scanner sc = new Scanner(System.in);
		 key = sc.nextInt();
		switch (key) {
		case 1:
           for(int i = 0;i<11;i++)
           {  
        	   System.out.println("enter country number: "+i);
        	   EastAsiaCountries counrty = addEast();
        	   addCountryInformation(counrty);
           }
			break;
		case 2:
            EastAsiaCountries country = getRecentlyEnteredInformation();
            System.out.printf("%10s %30s %20s %5s", "CODE",  "NAME", "TOTAL ARREA", "TERRAIN");
            System.out.println();
            System.out.printf("%10s %30s %20s %5s", country.getCountryCode(), country.getCountryName(),country.getTotalArea(), country.getCountryTerrain());
            System.out.println();
            
			break;
		case 3:
		   System.out.println("enter name to search:");
		   
		   String name= sc.next();
           List<EastAsiaCountries> searchCountries = searchInformationByName(name);
           System.out.printf("%10s %30s %20s %5s", "CODE",  "NAME", "TOTAL ARREA", "TERRAIN");
           
           for(int i = 0 ; i <searchCountries.size();i++)
           { EastAsiaCountries country2= searchCountries.get(i);
           System.out.println();
           System.out.printf("%10s %30s %20s %5s", country2.getCountryCode(), country2.getCountryName(),country2.getTotalArea(), country2.getCountryTerrain());

           }
			break;
		case 4 :
			List<EastAsiaCountries> sortCountries=sortInformationByAscendingOrder();
		    System.out.printf("%10s %30s %20s %5s", "CODE",  "NAME", "TOTAL ARREA", "TERRAIN");
			  for(int i = 0 ; i <sortCountries.size();i++)
	           { EastAsiaCountries country2= sortCountries.get(i);
	           System.out.println();
	           System.out.printf("%10s %30s %20s %5s", country2.getCountryCode(), country2.getCountryName(),country2.getTotalArea(), country2.getCountryTerrain());

	           }
			break;
		case 5:
			System.exit(0);
			break;

		default:
			break;
		}}
	}

	public static EastAsiaCountries addEast() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter code of country:");
		String code = sc.next();
		System.out.println("Enter name of country:");
		String name = sc.next();
		System.out.println("Enter total Area:");
		float area = sc.nextFloat();
		System.out.println("Enter terrain of country:  ");
		String countryTerrain = sc.next();
		EastAsiaCountries a = new EastAsiaCountries(countryTerrain);
		a.setCountryCode(code);
		a.setCountryName(name);
		a.setCountryTerrain(countryTerrain);
		a.setTotalArea(area);

		return a;

	}

	public static void addCountryInformation(EastAsiaCountries country) throws Exception {
		countries.add(country);
	}

	public static EastAsiaCountries getRecentlyEnteredInformation() throws Exception {
		return countries.get(countries.size()-1);
	}
    
	
	public static List<EastAsiaCountries> searchInformationByName(String name) throws Exception {
		List<EastAsiaCountries> counrties = new ArrayList<>();
		for (int i = 0; i < countries.size(); i++)

		{
			if (name.equals(countries.get(i).getCountryName())) {
				counrties.add(countries.get(i));

			}
		}
		return counrties;
	}

	public static List<EastAsiaCountries> sortInformationByAscendingOrder() throws Exception {
		Collections.sort(countries);
		return countries;
	}

}
