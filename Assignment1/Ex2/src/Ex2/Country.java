package Ex2;

public abstract class Country {
	protected String countryCode;
	protected String countryName;
	protected float totalArea;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public float getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(float totalArea) {
		this.totalArea = totalArea;
	}

	public Country() {
		super();
		// TODO Auto-generated constructor stub
	}

	abstract void display();
}
