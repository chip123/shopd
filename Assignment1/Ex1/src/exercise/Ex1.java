package exercise;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex1 {

	public static double calculate(double a, String operator, double b) {

		double c = 0;

		if (operator.equals("-")) {
			c = a - b;
		} else if (operator.equals("+")) {
			c = a + b;
		} else if (operator.equals("*")) {
			c = a * b;
		} else if (operator.equals("/")) {
			c = a / b;
		} else if (operator.equals("^")) {
			c = Math.pow(a, b);
		}

		return c;

	}

	public static String calculateBMI(double weight, double height) {
		int BMI = (int) ((weight) / (height * height));
		String s = null;
		if (BMI < 19) {
			s = "Dưới chuẩn";
		} else if (BMI > 19 && BMI < 25) {
			s = "Chuẩn";
		} else if (BMI > 25 && BMI < 30) {
			s = "Thừa cân";
		} else if (BMI > 30 && BMI < 40) {
			s = "Béo - nên giảm ";
		} else if (BMI > 40) {
			s = "Rất béo – cần giảm cân ngay";
		}
		return s;
	}

	public static boolean checkOperator(String operator) {

		if (operator.equals("+") || operator.equals("-") || operator.equals("*") || operator.equals("/")
				|| operator.equals("^"))

		{
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkIn(String string) {

		boolean numeric = true;

		try {
			Double num = Double.parseDouble(string);
		} catch (NumberFormatException e) {
			numeric = false;
		}

		if (numeric)
			return numeric;
		else
			return (!numeric);
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int key = 0;
		while (key != 3) {
			System.out.println("========= Calculator Program =========\r\n" + "1. Normal Calculator\r\n"
					+ "2. BMI Calculator\r\n" + "3. Exit\r\n" + "Please choice one option:");
			key = sc.nextInt();

			switch (key) {
			case 1:
				String operator = null;
				int a = 0;
				int b = 0;
				double memory = 0;

				System.out.println("enter a :");
				a = sc.nextInt();
				System.out.println("enter b:");
				b = sc.nextInt();
				while ((operator == null) || (!(operator.equals("=")))) {
					System.out.println("enter operator: ");
					operator = sc.next();
					if (checkOperator(operator) == true) {
						if (!(operator.equals("="))) {
							System.out.println("enter b:");
							b = sc.nextInt();
						}
						memory = calculate(memory, operator, b);
						if ((operator.equals("="))) {
							System.out.println("Result :" + memory);
						} else {
							System.out.println("Memory :" + memory);
						}
					} else {
						System.out.println("need to print operator +,-,*,/");
					}

				}
				// System.out.println("Result :" + memory);
				break;
			case 2:
				
				System.out.println("enter weight");
				double weight = sc.nextDouble();
				System.out.println("enter height");
				double height = sc.nextDouble();
				System.out.println("Result " + calculateBMI(weight, height));
					
				break;

			case 3:
				System.exit(0);
			default:
				break;
			}
		}
	}

}
