package exercise.enitities;

public class MathTeacher extends Teacher implements Actionable {
   private String mainSubject="Math";
   
   
   
	public MathTeacher(String designation, String collegename, String mainSubject) {
	super(designation, collegename);
	this.mainSubject = mainSubject;
}

	public MathTeacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MathTeacher(String designation, String collegename) {
		super(designation, collegename);
		// TODO Auto-generated constructor stub
	}

	public String getMainSubject() {
	return mainSubject;
}

public void setMainSubject(String mainSubject) {
	this.mainSubject = mainSubject;
}

	@Override
	public void toSchool() {
		// TODO Auto-generated method stub
		System.out.println("Math teacher go to school by car");
		
	}

	@Override
	public void teach() {
		// TODO Auto-generated method stub
		System.out.println("Teaching math subject");
		
	}
	public int sum(int number1,int number2)
	{
		return (number1+number2);
	}

	
}
