package exercise.enitities;

public abstract class Teacher {
private String designation="Teacher";
private String collegename="FU";
public String getDesignation() {
	return designation;
}
public void setDesignation(String designation) {
	this.designation = designation;
}
public String getCollegename() {
	return collegename;
}
public void setCollegename(String collegename) {
	this.collegename = collegename;
}
public Teacher(String designation, String collegename) {
	super();
	this.designation = designation;
	this.collegename = collegename;
}
public Teacher() {
	super();
	// TODO Auto-generated constructor stub
}
public abstract void teach();
public void teach(int duration)
{
	System.out.println("Teahing in "+duration+"minutes");}
}
