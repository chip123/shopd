package exercise.enitities;

public class EnglishTeacher extends Teacher implements Actionable {

	private String mainSubject="English";

	@Override
	public void toSchool() {
		// TODO Auto-generated method stub
		System.out.println("English Teacher go to school by motobike");
	}

	@Override
	public void teach() {
		// TODO Auto-generated method stub
		System.out.println("Teaching Enlish subject");
	}

	public String getMainSubject() {
		return mainSubject;
	}

	public void setMainSubject(String mainSubject) {
		this.mainSubject = mainSubject;
	}

	public EnglishTeacher(String designation, String collegename, String mainSubject) {
		super(designation, collegename);
		this.mainSubject = mainSubject;
	}

	public EnglishTeacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnglishTeacher(String designation, String collegename) {
		super(designation, collegename);
		// TODO Auto-generated constructor stub
	}

	public String translate(String en, String vi) {
		return en + " in VietNamese is " + vi;
	}

}
