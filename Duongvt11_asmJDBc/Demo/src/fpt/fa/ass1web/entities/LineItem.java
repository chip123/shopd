package fpt.fa.ass1web.entities;

public class LineItem {
	private int orderId;
	private int productId;
	private int totalPrice;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public LineItem(int orderId, int productId, int totalPrice) {
		super();
		this.orderId = orderId;
		this.productId = productId;
		this.totalPrice = totalPrice;
	}
	public LineItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "LineItem [orderId=" + orderId + ", productId=" + productId + ", totalPrice=" + totalPrice + "]";
	}
	
	

}
