package fpt.fa.ass1web.entities;

import java.util.Date;

public class Orders {
	private int OrderId;
	private String OrderDate;
	private int customerId;
	private int employeeId;
	private int total;
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}
	public String getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(String orderDate) {
		OrderDate = orderDate;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public Orders(int orderId, String orderDate, int customerId, int employeeId, int total) {
		super();
		OrderId = orderId;
		OrderDate = orderDate;
		this.customerId = customerId;
		this.employeeId = employeeId;
		this.total = total;
	}
	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Orders(String orderDate, int customerId, int employeeId, int total) {
		super();
		OrderDate = orderDate;
		this.customerId = customerId;
		this.employeeId = employeeId;
		this.total = total;
	}
	
	
	public Orders(int orderId, String orderDate, int customerId, int employeeId) {
		super();
		OrderId = orderId;
		OrderDate = orderDate;
		this.customerId = customerId;
		this.employeeId = employeeId;
	}
	@Override
	public String toString() {
		return "Orders [OrderId=" + OrderId + ", OrderDate=" + OrderDate + ", customerId=" + customerId
				+ ", employeeId=" + employeeId + ", total=" + total + "]";
	}
	
	

}
