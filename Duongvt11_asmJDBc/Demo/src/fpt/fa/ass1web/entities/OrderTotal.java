package fpt.fa.ass1web.entities;

public class OrderTotal {
	
	private int orderTotal;
	
	private int orderId;

	public int getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(int orderTotal) {
		this.orderTotal = orderTotal;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public OrderTotal(int orderTotal, int orderId) {
		super();
		this.orderTotal = orderTotal;
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "OrderTotal [orderTotal=" + orderTotal + ", orderId=" + orderId + "]";
	}

	public OrderTotal() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
