package fpt.fa.ass1web.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fpt.fa.ass1web.entities.*;
import fpt.fa.ass1web.utils.DBUtils;

public class SaleDAO {
	DBUtils db = new DBUtils();

	public List<Customer> listAllCustomers() {
		List<Customer> customers = new ArrayList<>();
		try {
			// connnect to database 'testdb'

			Connection conn = db.getConnection();
			// crate statement
			Statement stmt = conn.createStatement();
			// get data from table 'student'
			ResultSet rs = stmt.executeQuery("select * from customer");

			// show data
			while (rs.next()) {
				Customer customer = new Customer();
				customer.setCustomerId(rs.getInt(1));
				customer.setCustomerName(rs.getString(2));
				customers.add(customer);
			}

			// close connection
			conn.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return customers;
	}

	public List<Orders> listCustomerOrders() {
		List<Orders> orders = new ArrayList<>();
		try {
			// connnect to database 'testdb'

			Connection conn = db.getConnection();
			// crate statement
			Statement stmt = conn.createStatement();
			// get data from table 'student'
			ResultSet rs = stmt.executeQuery("select * from Orders");

			// show data
			while (rs.next()) {
				Orders order = new Orders();
				order.setOrderId(rs.getInt(1));
				order.setOrderDate(rs.getString(2));
				order.setCustomerId(rs.getInt(3));
				order.setEmployeeId(rs.getInt(4));
				order.setTotal(rs.getInt(5));

				orders.add(order);
			}

			// close connection
			conn.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return orders;
	}

//
	public List<LineItem> listLineItemsForOrder() {
		List<LineItem> lineItems = new ArrayList<>();
		try {
			// connnect to database 'testdb'

			Connection conn = db.getConnection();
			// crate statement
			Statement stmt = conn.createStatement();
			// get data from table 'student'
			ResultSet rs = stmt.executeQuery("select * from LineItem");

			// show data
			while (rs.next()) {
				LineItem lineItem = new LineItem();
				lineItem.setOrderId(rs.getInt(1));
				lineItem.setProductId(rs.getInt(2));
				lineItem.setTotalPrice(rs.getInt(3));

				lineItems.add(lineItem);
			}

			// close connection
			conn.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return lineItems;
	}

//
	public List<OrderTotal> computedOrderTotal() {
		SaleDAO saleDao = new SaleDAO();
		List<LineItem> lineItems = saleDao.listLineItemsForOrder();
		List<OrderTotal> orderTotals = new ArrayList<>();
		for (int i = 0; i < lineItems.size(); i++) {

			int orderId = lineItems.get(i).getOrderId();
			if (checkExistOrderInLineItem(orderId, orderTotals) == true) {
				int total = lineItems.get(i).getTotalPrice() + lineItems.get(orderId).getTotalPrice();
				OrderTotal orderTotal = new OrderTotal();
				orderTotal.setOrderId(orderId);
				orderTotal.setOrderTotal(total);
				orderTotals.set(orderId - 1, orderTotal);
			} else {
				int total = lineItems.get(i).getTotalPrice();
				OrderTotal orderTotal = new OrderTotal();
				orderTotal.setOrderId(orderId);
				orderTotal.setOrderTotal(total);
				orderTotals.add(orderTotal);
			}

		}
		return orderTotals;
	}

	public int computeTotalByOrderID(int orderId) {
     List<LineItem> lineItems = new ArrayList<>();
     int total = 0 ; 
		try {
			Connection connection = db.getConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("select * from LineItem where orderId = "+orderId);
			while(rs.next())
			{
				
				int productId = rs.getInt(2);
				int totalPrice = rs.getInt(3);
				LineItem lineItem = new LineItem(orderId,productId,totalPrice);
				lineItems.add(lineItem);
				
			}
			for(int i = 0 ;i<lineItems.size();i++)
			{
				
				total +=lineItems.get(i).getTotalPrice();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return total;

	}

	public boolean checkExistOrderInLineItem(int orderID, List<OrderTotal> lineItems) {
		boolean exist = false;
		for (int i = 0; i < lineItems.size(); i++) {
			if (orderID == lineItems.get(i).getOrderId()) {
				exist = true;
			}
		}
		return exist;
	}

	public void updateTotalOrder(int id, int total) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "update Orders set total=" + total+ "where orderId = " +id;
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void addCustomer(Customer customer) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "insert into Customer values ('" + customer.getCustomerName() + "')";
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void deleteCustomer(int customerId) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "delete from Customer where CustomerId=" + customerId;
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void updateCustomer(Customer customer) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "update Customer set customerName='" + customer.getCustomerName() + "' where CustomerId="
					+ customer.getCustomerId();
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void createLineItem(LineItem lineItem) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "insert into LineItem values (" + lineItem.getOrderId() + "," + lineItem.getProductId() + ","
					+ lineItem.getTotalPrice() + ")";
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void createOrder(Orders order) {
		try {
			Connection connection = db.getConnection();
			Statement statement = connection.createStatement();
			String sql = "insert into Orders values ('" + order.getOrderDate() + "'," + order.getCustomerId() + ","
					+ order.getEmployeeId() + "," + order.getTotal() + ")";
			statement.execute(sql);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
