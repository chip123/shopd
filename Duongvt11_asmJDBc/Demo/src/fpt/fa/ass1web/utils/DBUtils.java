package fpt.fa.ass1web.utils;

 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 
public class DBUtils {
    private static String DB_URL = "jdbc:sqlserver://localhost:1433;"
            + "databaseName=rr;";
    private static String USER_NAME = "sa";
    private static String PASSWORD = "12";
 
    /**
     * main
     * 
     * @author viettuts.vn
     * @param args
     */
   

 
    /**
     * create connection 
     * 
     * @author viettuts.vn
     * @param dbURL: database's url
     * @param userName: username is used to login
     * @param password: password is used to login
     * @return connection
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return conn;
    }
    
}
