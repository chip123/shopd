package fpt.fa.ass1web.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fpt.fa.ass1web.dao.SaleDAO;
import fpt.fa.ass1web.entities.Customer;
import fpt.fa.ass1web.entities.LineItem;
import fpt.fa.ass1web.entities.OrderTotal;
import fpt.fa.ass1web.entities.Orders;

public class ShopManagement {
	static SaleDAO saleDao = new SaleDAO();

	public static void main(String[] args) {
		int key =0;
		while(key!=10) {
		System.out.println(
				"1. List all customers consist customer id, customer name, this function must to returns a list \r\n"
						+ "with all the customers in the order database (listAllCustomers method). \r\n"
						+ "2. List all orders consist order id, order date, customer id, employee id, total for a customer, \r\n"
						+ "returns a String with all the orders for a given customer id (listCustomerOrders method). \r\n"
						+ "3. List all lineitems for an order, returns a list with all line items for a given order id \r\n"
						+ "(listLineItemsForOrder method). \r\n"
						+ "4. Computer order total, returns a List with a row containing the computed order total from the \r\n"
						+ "line items (named as orderTotal) for a given order id \r\n"
						+ "5. Adds a customer into databases. \r\n"
						+ "6. Deletes a customer from the databases, make sure to also delete Orders and OrderedProducts \r\n"
						+ "for the customer deleted.  \r\n" + "7. Updates a customer in the database. \r\n"
						+ "8. Creates an order in the database. \r\n" + "9. Creates a lineitem in the database \r\n"
						+ "10.Exit \r\n"
						+"11. Update total for order");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter action number:");
		 key = sc.nextInt();
  
		switch (key) {
	     
		case 1:
			List<Customer>customers = saleDao.listAllCustomers();
			
            for(int i = 0;i<customers.size();i++)
            {
            	System.out.println(customers.get(i).toString());
            }
			break;

		case 2:
			
			List<Orders> orders = saleDao.listCustomerOrders();
			for(int i =0 ; i<orders.size();i++)
			{
				System.out.println(orders.get(i).toString());
			}

			break;
		case 3:
			List<LineItem> lineItems = saleDao.listLineItemsForOrder();
            for ( int i = 0 ;i<lineItems.size();i++)
            {
              System.out.println(lineItems.get(i).toString());	
            }
			break;
		case 4:
			List<Orders> orders2 = saleDao.listCustomerOrders();
			 int totalSale = 0;
			for(int i = 0 ; i <orders2.size();i++)
			{  
				 totalSale += saleDao.computeTotalByOrderID(orders2.get(i).getOrderId());
				System.out.println("total sale:"+totalSale);
			}
			break;
		case 5:
			System.out.println("Enter name for customer");
			String name = sc.next();
			Customer customer = new Customer();
			customer.setCustomerName(name);
			saleDao.addCustomer(customer);

			break;
		case 6:
			System.out.println("enter id of customer to delete");
			int id = sc.nextInt();
			saleDao.deleteCustomer(id);

			break;
		case 7:
			System.out.println("enter id of customer to update ");
			int idCustomer = sc.nextInt();
			System.out.println("enter new name for customer ");
			String nameCustomer = sc.next();
			Customer customer2 = new Customer(idCustomer, nameCustomer);
			saleDao.updateCustomer(customer2);
			break;
		case 8:			
			System.out.println("enter orderId");
			int orderId1=sc.nextInt();
			System.out.println("enter order date :");
			String date = sc.next();
			System.out.println("enter customer id for order");
			int id3 = sc.nextInt();
			System.out.println("enter employee id for order ");
			int emId = sc.nextInt();
			
			Orders order = new Orders(orderId1,date,id3,emId);
			saleDao.createOrder(order);
			
			break;
		case 9:
			System.out.println("enter orderId");
			int orderId = sc.nextInt();
			System.out.println("enetr productId");
			int productId =sc.nextInt();
			System.out.println("enter totalPrice");
			int total = sc.nextInt();
			LineItem lineItem = new LineItem(orderId,productId,total);
			saleDao.createLineItem(lineItem);
			
			break;
		case 10:
			System.exit(0);
			

			break;
		case 11:
			System.out.println("enter orderID to update");
			int orderIdToUpdate= sc.nextInt();
			int total1= saleDao.computeTotalByOrderID(orderIdToUpdate);		
			saleDao.updateTotalOrder( orderIdToUpdate, total1);
			List<Orders> ordersAfterUpdate = saleDao.listCustomerOrders();
			for(int i= 0 ; i <ordersAfterUpdate.size();i++)
			{System.out.println(ordersAfterUpdate.get(i).toString());}
			

		default:
			break;
		}
	}}}


