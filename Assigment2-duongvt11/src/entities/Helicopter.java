package entities;

public class Helicopter extends Airplane{
	 
	private int range ;

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public Helicopter(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			 int range) {
		super(iD, model, planeType, cruiseSpeed, emptyWeight, maxTakeoffWeight);
		this.range = range;
		
	}

	public Helicopter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Helicopter(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			String flyMethod) {
		super(iD, model, planeType, cruiseSpeed, emptyWeight, maxTakeoffWeight, flyMethod);
		// TODO Auto-generated constructor stub
	}
	
	
	
}
