package entities;

import java.io.Serializable;

public abstract class Airplane implements Serializable {
	 protected String ID;
	 protected String model;
	 protected String planeType;
	 protected int cruiseSpeed;
	 protected int emptyWeight;
	 protected int maxTakeoffWeight;
	 protected String flyMethod;
	 protected String airportID;
	public String getAirportID() {
		return airportID;
	}
	public void setAirportID(String airportID) {
		this.airportID = airportID;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getPlaneType() {
		return planeType;
	}
	public void setPlaneType(String planeType) {
		this.planeType = planeType;
	}
	public int getCruiseSpeed() {
		return cruiseSpeed;
	}
	public void setCruiseSpeed(int cruiseSpeed) {
		this.cruiseSpeed = cruiseSpeed;
	}
	public int getEmptyWeight() {
		return emptyWeight;
	}
	public void setEmptyWeight(int emptyWeight) {
		this.emptyWeight = emptyWeight;
	}
	public int getMaxTakeoffWeight() {
		return maxTakeoffWeight;
	}
	public void setMaxTakeoffWeight(int maxTakeoffWeight) {
		this.maxTakeoffWeight = maxTakeoffWeight;
	}
	public String getFlyMethod() {
		return flyMethod;
	}
	public void setFlyMethod(String flyMethod) {
		this.flyMethod = flyMethod;
	}
	public Airplane(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			String flyMethod) {
		super();
		ID = iD;
		this.model = model;
		this.planeType = planeType;
		this.cruiseSpeed = cruiseSpeed;
		this.emptyWeight = emptyWeight;
		this.maxTakeoffWeight = maxTakeoffWeight;
		this.flyMethod = flyMethod;
	}
	public Airplane() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Airplane(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			String flyMethod, String airportID) {
		super();
		ID = iD;
		this.model = model;
		this.planeType = planeType;
		this.cruiseSpeed = cruiseSpeed;
		this.emptyWeight = emptyWeight;
		this.maxTakeoffWeight = maxTakeoffWeight;
		this.flyMethod = flyMethod;
		this.airportID = airportID;
	}
	public Airplane(String iD2, String model2, String planeType2, int cruiseSpeed2, int emptyWeight2,
			int maxTakeoffWeight2) {
		// TODO Auto-generated constructor stub
	}
	 
	 

}
