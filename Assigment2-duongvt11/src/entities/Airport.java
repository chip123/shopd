package entities;

import java.io.Serializable;
import java.util.List;

public class Airport implements Serializable{
	 private String ID;
	 private String name;
	 private int runwaySize;
	 private String maxFixedWingParkingPlace;
 String maxRotatedWingParkingPlace;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRunwaySize() {
		return runwaySize;
	}
	public void setRunwaySize(int runwaySize) {
		this.runwaySize = runwaySize;
	}
	public String getMaxFixedWingParkingPlace() {
		return maxFixedWingParkingPlace;
	}
	public void setMaxFixedWingParkingPlace(String maxFixedWingParkingPlace) {
		this.maxFixedWingParkingPlace = maxFixedWingParkingPlace;
	}

	public String getMaxRotatedWingParkingPlace() {
		return maxRotatedWingParkingPlace;
	}
	public void setMaxRotatedWingParkingPlace(String maxRotatedWingParkingPlace) {
		this.maxRotatedWingParkingPlace = maxRotatedWingParkingPlace;
	}
	public Airport(String iD, String name, int runwaySize, String maxFixedWingParkingPlace,
			List<String> fixedWingAirplaneID, String maxRotatedWingParkingPlace, List<String> helicopterID) {
		super();
		ID = iD;
		this.name = name;
		this.runwaySize = runwaySize;
		this.maxFixedWingParkingPlace = maxFixedWingParkingPlace;
		
		this.maxRotatedWingParkingPlace = maxRotatedWingParkingPlace;
	
	}
	public Airport() {
		super();
		// TODO Auto-generated constructor stub
	}
	 
	 

}
