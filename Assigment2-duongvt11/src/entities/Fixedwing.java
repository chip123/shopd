package entities;

public class Fixedwing extends Airplane{
	private int minNeededRunwaySize ;

	public int getMinNeededRunwaySize() {
		return minNeededRunwaySize;
	}

	public void setMinNeededRunwaySize(int minNeededRunwaySize) {
		this.minNeededRunwaySize = minNeededRunwaySize;
	}

	public Fixedwing(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			 int minNeededRunwaySize) {
		super(iD, model, planeType, cruiseSpeed, emptyWeight, maxTakeoffWeight);
		this.minNeededRunwaySize = minNeededRunwaySize;
			}

	public Fixedwing() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Fixedwing(String iD, String model, String planeType, int cruiseSpeed, int emptyWeight, int maxTakeoffWeight,
			String flyMethod) {
		super(iD, model, planeType, cruiseSpeed, emptyWeight, maxTakeoffWeight, flyMethod);
		// TODO Auto-generated constructor stub
	}
	
	

}
