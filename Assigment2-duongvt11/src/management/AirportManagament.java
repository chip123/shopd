package management;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entities.Airplane;
import entities.Airport;
import entities.Fixedwing;
import entities.Helicopter;

public class AirportManagament {
	static List<Airport> airports = new ArrayList<Airport>();
	static List<Airplane> airplanes = new ArrayList<Airplane>();
	static List<Fixedwing> fixedWings = new ArrayList<Fixedwing>();
	static List<Helicopter> helicopter = new ArrayList<Helicopter>();
	static List<Fixedwing> suitfixedWings = null;

	public static void saveAirport(List<Airport> airport) throws IOException {
		FileOutputStream fos = new FileOutputStream("airport.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(airport);
		oos.close();

	}

	public static List<Airport> getAllAirport() throws IOException, ClassNotFoundException {
		FileInputStream fos = new FileInputStream("airport.txt");
		ObjectInputStream oos = new ObjectInputStream(fos);
		List<Airport> airport = null;
		airport = (List<Airport>) oos.readObject();
		oos.close();
		return airport;
	}

	public static void createAirport() throws IOException {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter ID");
		String ID = sc.next();
		if (validAirId(ID)) {
			System.out.println("enter name");
			String name = sc.next();
			System.out.println("enter runwaySize");
			int runwaySize = Integer.parseInt(sc.next());
			System.out.println("enter maxFixedWingParkingPlace");
			String maxFixedWingParkingPlace = sc.next();

			System.out.println("enter maxRotatedWingParkingPlace");
			String maxRotatedWingParkingPlace = sc.next();

			Airport airport = new Airport();

			airport.setID(ID);
			airport.setMaxFixedWingParkingPlace(maxFixedWingParkingPlace);
			airport.setMaxRotatedWingParkingPlace(maxRotatedWingParkingPlace);
			airport.setName(name);
			airport.setRunwaySize(runwaySize);
			airports.add(airport);
			saveAirport(airports);
		} else {
			System.out.println("airport id form : �AP� followed by 5 digits");
		}
	}

	public static void deleteAirport(String heliId) {

	}

	public static Airport getAirportByID(String idAirport) {
		Airport airport = null;

		for (int i = 0; i < airports.size(); i++) {
			if (idAirport.equals(airports.get(i).getID())) {

				airport = airports.get(i);

			}

		}
		return airport;

	}

	public static void createFixedWing() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter ID:");
		String ID = sc.next();
		if (validFWId(ID)) {
			System.out.println("enter model");
			String model = sc.next();
			if(model.length()<40) {
			System.out.println("choose plane type");
			displayFixedWingType();
			String key = sc.next();
			String planeType = choosePlaneType(key);

			System.out.println("enter cruise speed");
			int cruiseSpeed = sc.nextInt();
			System.out.println("enter empty weight");
			int weight = sc.nextInt();
			System.out.println("enter max takeoff weight");
			int maxTakeoffWeight = sc.nextInt();
			System.out.println("enter min needed runway size");
			int minNeededRunwaySize = sc.nextInt();

			Fixedwing fw = new Fixedwing();

			fw.setCruiseSpeed(cruiseSpeed);

			fw.setFlyMethod("fixed wing");
			fw.setID(ID);
			fw.setMaxTakeoffWeight(maxTakeoffWeight);
			fw.setModel(model);
			fw.setPlaneType(planeType);
			fw.setMinNeededRunwaySize(minNeededRunwaySize);
			fixedWings.add(fw);}
			else {
				System.out.println("length of model need to be smaller than 40");
			}
		} else {
			System.out.println("�FW� for airport, followed by 5 digits");
		}
	}

	public static void displayHeli() {
		System.out.printf("%10s %30s", "HeliId", "AIRPORT ID");
		System.out.println();

		for (int i = 0; i < helicopter.size(); i++) {
			System.out.printf("%10s %30s", helicopter.get(i).getID(), helicopter.get(i).getAirportID());
			System.out.println();

		}
	}

	public static void displayFixedWings() {
		System.out.printf("%10s %30s %20s %5s", "FIXED WING ID", "PLANE TYPE", "MIN NEEDED SIZE", "AIRPORT ID");
		System.out.println();

		for (int i = 0; i < fixedWings.size(); i++) {
			System.out.printf("%10s %30s %20s %5s", fixedWings.get(i).getID(), fixedWings.get(i).getPlaneType(),
					fixedWings.get(i).getMinNeededRunwaySize(), fixedWings.get(i).getAirportID());
			System.out.println();

		}
	}

	public static void createHeliCopter() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter ID:");
		String ID = sc.next();
		if (validHeliId(ID)) {

			System.out.println("enter model");
			String model = sc.next();
			System.out.println("enter plane type");
			String planeType = sc.next();
			System.out.println("enter cruise speed");
			int cruiseSpeed = sc.nextInt();
			System.out.println("enter empty weight");
			int weight = sc.nextInt();
			System.out.println("enter max takeoff weight");
			int maxTakeoffWeight = sc.nextInt();
			System.out.println("enter range");
			int range = sc.nextInt();

			Helicopter fw = new Helicopter();

			fw.setCruiseSpeed(cruiseSpeed);
			fw.setEmptyWeight(weight);

			fw.setID(ID);
			fw.setMaxTakeoffWeight(maxTakeoffWeight);
			fw.setModel(model);
			fw.setPlaneType(planeType);
			fw.setRange(range);
			fw.setFlyMethod("rotated wing");
			helicopter.add(fw);
		} else {
			System.out.println("valid HeliId: RW  followed by 5 digits");
		}
	}

	public static List<Airplane> getAirplanes(String order) {
		return null;
	}

	public static List<Fixedwing> getFixedWing(int airportSize) {
		List<Fixedwing> suitfw = new ArrayList<>();
		for (int i = 0; i < fixedWings.size(); i++) {
			if (fixedWings.get(i).getMinNeededRunwaySize() < airportSize && fixedWings.get(i).getAirportID() == null) {
				suitfw.add(fixedWings.get(i));
			}
		}
		return suitfw;
	}

	public static Fixedwing getFixedSwingById(String ID) {
		Fixedwing fw = null;
		for (int i = 0; i < fixedWings.size(); i++) {
			if (fixedWings.get(i).getID().equals(ID)) {
				fw = fixedWings.get(i);
			}
		}
		return fw;
	}

	public static int getFixedSwingIdxById(String ID) {
		Fixedwing fw = null;
		int idx = fixedWings.size();
		for (int i = 0; i < fixedWings.size(); i++) {
			if (fixedWings.get(i).getID().equals(ID)) {
				fw = fixedWings.get(i);
				idx = i;
			}
		}
		return idx;
	}

	public static Helicopter getHelicopterById(String ID) {
		Helicopter heli = null;
		for (int i = 0; i < helicopter.size(); i++) {
			if (helicopter.get(i).getID().equals(ID)) {
				heli = helicopter.get(i);

			}
		}
		return heli;
	}

	public static Airport getAirportById(String ID) {
		Airport airport = null;
		for (int i = 0; i < airports.size(); i++) {
			if (airports.get(i).getID().equals(ID)) {
				airport = airports.get(i);

			}
		}
		return airport;
	}

	public static int getHelicopterIdxById(String ID) {
		Helicopter heli = null;
		int idx = helicopter.size();
		for (int i = 0; i < helicopter.size(); i++) {
			if (helicopter.get(i).getID().equals(ID)) {
				heli = helicopter.get(i);
				idx = i;
			}
		}
		return idx;
	}

	public static List<Helicopter> getHelicopter() {
		List<Helicopter> heliNotInAirport = new ArrayList<>();
		for (int j = 0; j < helicopter.size(); j++) {
			if (helicopter.get(j).getAirportID() == null) {
				heliNotInAirport.add(helicopter.get(j));
			}
		}
		return heliNotInAirport;
	}

	public static void addFixedToAirport(String airplaneID, String airportID) {

		Fixedwing fw = new Fixedwing();
		for (int i = 0; i < fixedWings.size(); i++) {
			if (airplaneID.equals(fixedWings.get(i).getID())) {
				fw = fixedWings.get(i);
				fw.setAirportID(airportID);
				fixedWings.set(i, fw);
			}
		}

	}

	public static void addHeliToAirport(String airplaneID, String airportID) {

		Helicopter fw = new Helicopter();
		for (int i = 0; i < helicopter.size(); i++) {
			if (airplaneID.equals(helicopter.get(i).getID())) {
				fw = helicopter.get(i);
				fw.setAirportID(airportID);
				helicopter.set(i, fw);
			}
		}

	}

	public static void removeFromAirport(String helicpterID) {
		Helicopter heli = getHelicopterById(helicpterID);
		int idx = getHelicopterIdxById(helicpterID);
//		System.out.println(idx);
		heli.setAirportID(" ");

		helicopter.set(idx, heli);

	}

	public static void displayFixedWingType() {
		System.out.println("1. CAG (Cargo)\r\n" + "2. LGR (Long range) \r\n" + "3. PRV (Private)");

	}

	public static String choosePlaneType(String key) {
		String planeType = null;
		if ("1".equals(key)) {
			planeType = "CAG";
		} else if ("2".equals(key)) {
			planeType = "LGR";
		} else if ("3".equals(key)) {
			planeType = "PRV";
		}
		return planeType;
	}

	public static void updatefixedWings(String order, String fixedWingId)

	{
		Scanner sc = new Scanner(System.in);
		Fixedwing fw = getFixedSwingById(fixedWingId);
		int indexFW = getFixedSwingIdxById(fixedWingId);
		if ("1".equals(order)) {
			displayFixedWingType();
			System.out.println("choose number for plane type");
			String type = sc.next();
			fw.setPlaneType(choosePlaneType(type));
			fixedWings.set(indexFW, fw);
		} else if ("2".equals(order)) {
			System.out.println("enter min needed size ");
			String min = sc.next();

			fw.setMinNeededRunwaySize(Integer.parseInt(min));

			if (fw.getAirportID() != null) {
				if (!(Integer.parseInt(min) > getAirportByID(fw.getAirportID()).getRunwaySize())) {

					fixedWings.set(indexFW, fw);
					// }
				} else {
					System.out.println(
							"min needed runway size of airplane need to smaller than min of max run way size airport");
				}
			} else {
				fixedWings.set(indexFW, fw);
			}

		}

	}

	private static Pattern pattern;

	// non-static Matcher object because it's created from the input String
	private static Matcher matcher;
	private static final String HELI_REGEX = "^RW\\d\\d\\d\\d\\d";
	private static final String FW_REGEX = "^FW\\d\\d\\d\\d\\d";
	private static final String AIRPORT_REGEX = "^AP\\d\\d\\d\\d\\d";

	public static boolean validHeliId(String heliId) {
		pattern = Pattern.compile(HELI_REGEX, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(heliId);
		return matcher.matches();
	}

	public static boolean validFWId(String fwId) {
		pattern = Pattern.compile(FW_REGEX, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(fwId);
		return matcher.matches();
	}

	public static boolean validAirId(String airId)

	{
		pattern = Pattern.compile(AIRPORT_REGEX, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(airId);
		return matcher.matches();
	}

	public static void displayAirport() throws ClassNotFoundException, IOException {
//		List<Airport> airportFromFle=getAll();
//		for (int i = 0; i < airports.size(); i++) {
//			System.out.println("test");
//			System.out.printf("%10s %30s", airportFromFle.get(i).getID(), airportFromFle.get(i).getName());
//
//		}
		System.out.printf("%10s %30s", "ID", "NAME");
		System.out.println();

		for (int i = 0; i < airports.size(); i++) {
			System.out.printf("%10s %30s", airports.get(i).getID(), airports.get(i).getName());

		}
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Scanner sc = new Scanner(System.in);
		int key = 0;
		while (key != 8) {
			System.out.println("1.create airport\r\n" + "2.create fixed wing\r\n" + "3.create helicopter\r\n"
					+ "4.add fixed wing which not participate and has the min needed runway size shorter than the airport runway size to an airport.\r\n"
					+ "5.add helicopter which not participate to an airport.  \r\n"
					+ "6.remove helicopter from airport \r\n"
					+ "7.change plane type and min needed runway size of fixed wing airplane.");

			key = sc.nextInt();
			switch (key) {
			case 1:
				createAirport();

				break;
			case 2:
				createFixedWing();
				break;
			case 3:
				createHeliCopter();
				break;
			case 4:
				System.out.println("airport list");
				displayAirport();
				System.out.println("choose airportID to add");
				String airportID = sc.next();

				System.out.println("fixed wing list");
				Airport airport = getAirportByID(airportID);
				suitfixedWings = getFixedWing(airport.getRunwaySize());
				if (suitfixedWings.size() > 0) {
					for (int j = 0; j < suitfixedWings.size(); j++) {
						System.out.println(suitfixedWings.get(j).getID() + "  " + suitfixedWings.get(j).getModel());
					}
					System.out.println("choose airplaneID to add");
					String id = sc.next();

					addFixedToAirport(id, airportID);
					displayFixedWings();
				} else {
					System.out.println("no suit fixed wings to add to airport");
				}
				break;
			case 5:
				System.out.println("helicopter list");
				List<Helicopter> helicopterNotInAirport = getHelicopter();
				if (helicopterNotInAirport.size() > 0) {
					for (int j = 0; j < helicopterNotInAirport.size(); j++) {
						System.out.println(
								helicopterNotInAirport.get(j).getID() + "  " + helicopterNotInAirport.get(j).getModel()
										+ " " + helicopterNotInAirport.get(j).getFlyMethod());
					}
					System.out.println("choose airplaneID to add");
					String idHeli = sc.next();
					System.out.println("airport list");
					displayAirport();
					System.out.println("choose airportID to add");
					String airportIDH = sc.next();

					addHeliToAirport(idHeli, airportIDH);
					displayHeli();
				} else {
					System.out.println("no suit helicopter to add to airport");
				}
				break;
			case 6:
				if (helicopter.size() > 0) {
					System.out.println("helicopter list");

					for (int j = 0; j < helicopter.size(); j++) {
						System.out.println(helicopter.get(j).getID() + "  " + helicopter.get(j).getModel() + " "
								+ helicopter.get(j).getFlyMethod());
					}
					System.out.println("choose airplaneID to remove");
					String idH = sc.next();
					removeFromAirport(idH);
					displayHeli();
				} else {
					System.out.println("no helicopter to add to airport");
				}
				break;
			case 7:
				if (fixedWings.size() > 0) {
					System.out.println("fixed wings list");
					for (int k = 0; k < fixedWings.size(); k++) {
						System.out.println(fixedWings.get(k).getID() + "   " + fixedWings.get(k).getModel());

					}
					System.out.println("choose fixed wings id  to update ");
					String ID = sc.next();
					System.out.println("update  plane type(1) or min needed runway size(2) ?");
					String order = sc.next();

					updatefixedWings(order, ID);
					displayFixedWings();
				} else {
					System.out.println("no fixed wingd to update");
				}
				break;
			case 8:
				System.exit(0);

			default:
				break;
			}
		}
	}

}
