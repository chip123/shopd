package com.management;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.entities.Department;
import com.entities.Employee;
import com.entities.HourlyEmployee;
import com.entities.SalariedEmployee;

public class DepartmentManagement {
	private static List<Department> departments = new ArrayList<>();
	private static List<HourlyEmployee> hourlyEmployees = new ArrayList<>();
	private static List<SalariedEmployee> salariedEmployees = new ArrayList<>();
	private static List<Employee> employees = new ArrayList<>();

	public static void inputDepartment() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter deparment name: ");
		String departmentName = sc.next();
		Department department = new Department();
		department.setDepartmentName(departmentName);
		departments.add(department);
	}

	public static void displayDepartment() {
		for (int i = 0; i < departments.size(); i++) {
			System.out.println("Department name " + departments.get(i).getDepartmentName());
		}
	}

	public static void inputEmployee() {
		Scanner sc = new Scanner(System.in);
		System.out.println("choose type of employee(1 or 2) to add");
		System.out.println("1.Input Salaried Employee\r\n" + "2.HourlyEmployee");
		String type = sc.next();

		System.out.println("enter ssn");
		String ssn = sc.next();
		System.out.println("enter firstName");
		String firstName = sc.next();
		System.out.println("enter lastName");
		String lastName = sc.next();
		String birthDate = " ";
		while (!(validateBithDate(birthDate))) {
			System.out.println("enter birthDate dd-MM-yyyy");
			birthDate = sc.next();
		}
		String phone = "";
		while (!(validatePhone(phone))) {
			System.out.println("enter phone ");
			phone = sc.next();
		}
		String email = "";
		while (!(validateEmail(email))) {
			System.out.println("enter email ");
			email = sc.next();
		}
		if ("1".equals(type)) {
			System.out.println("enter commisstionRate");
			double commisstionRate = sc.nextDouble();
			System.out.println("enter grossSales");
			double grossSales = sc.nextDouble();
			System.out.println("enter basicSalary");
			double basicSalary = sc.nextDouble();
			SalariedEmployee salariedEmployee = new SalariedEmployee();
			salariedEmployee.setBasicSalary(basicSalary);
			salariedEmployee.setBirthDate(birthDate);
			salariedEmployee.setCommisstionRate(commisstionRate);
			// salariedEmployee.setdepartmentName(departmentName);
			salariedEmployee.setEmail(email);
			salariedEmployee.setFirstName(firstName);
			salariedEmployee.setGrossSales(grossSales);
			salariedEmployee.setLastName(lastName);
			salariedEmployee.setPhone(phone);
			salariedEmployee.setSsn(ssn);
			salariedEmployees.add(salariedEmployee);
			employees.add(salariedEmployee);

		} else if ("2".equals(type)) {
			System.out.println("enter rate");
			String rate = sc.next();
			System.out.println("enter working hour");
			String workingHour = sc.next();
			HourlyEmployee hourlyEmployee = new HourlyEmployee();
			hourlyEmployee.setBirthDate(birthDate);
			hourlyEmployee.setEmail(email);
			hourlyEmployee.setFirstName(firstName);
			hourlyEmployee.setLastName(lastName);
			hourlyEmployee.setPhone(phone);
			hourlyEmployee.setRate(Double.parseDouble(rate));
			hourlyEmployee.setSsn(ssn);
			hourlyEmployee.setWorkingHours(Double.parseDouble(workingHour));
			hourlyEmployees.add(hourlyEmployee);
			employees.add(hourlyEmployee);

		}
	}

	public static void displayEmployeeByClassify() {

		System.out.println("List employees");
		displaySalariedEmployee();
		displayHourlyEmployee();
	}

	public static void displayEmByDep() {
		for (int i = 0; i < employees.size(); i++) {
			System.out.println("Department name : " + employees.get(i).getDepartmentName());
			List<Employee> em = getEmployeesByDepartment(employees.get(i).getDepartmentName());
			for (int j = 0; j < em.size(); j++) {
				System.out.println("SSn" + em.get(j).getSsn());
			}

		}
	}

	public static List<Employee> getEmployeesByDepartment(String departmentName) {
		List<Employee> searchEms = new ArrayList<>();
		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getDepartmentName().equals(departmentName)) {
				searchEms.add(employees.get(i));
			}
		}
		return searchEms;
	}

	public static void addEmployeeToDepartment() {

		Scanner sc = new Scanner(System.in);

		displayEmployeeByClassify();
		System.out.println("enter ssn's employee to add employee to department ");
		String ssn = sc.next();
		Employee em = getEmployeeBySsn(ssn);

		displayDepartment();
		System.out.println("enter department name to add employee");
		String departName = sc.next();
		em.setdepartmentName(departName);
		displayEmployeeByClassify();

	}

	public static Employee getEmployeeBySsn(String ssn) {

		Employee em = null;
		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getSsn().equals(ssn)) {
				em = employees.get(i);
			}
		}
		return em;
	}

	public static void displaySalariedEmployee() {
		System.out.println("List Salaried Employees");
		for (int i = 0; i < salariedEmployees.size(); i++) {
			System.out.println("SSN " + salariedEmployees.get(i).getSsn() + "Department "
					+ salariedEmployees.get(i).getDepartmentName());
		}
	}

	public static void displayHourlyEmployee() {
		System.out.println("List DisplayHourly Employee");
		for (int i = 0; i < hourlyEmployees.size(); i++) {
			System.out.println("SSN " + hourlyEmployees.get(i).getSsn() + "Department "
					+ hourlyEmployees.get(i).getDepartmentName());
		}
	}

	public static void searchEmployeeByDepartment() {
		Scanner sc = new Scanner(System.in);
		displayDepartment();
		System.out.println("enter department to search");
		String departmentName = sc.next();
		List<Employee> suitEmployees = new ArrayList<>();
		Employee searchEmployee = null;

		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getDepartmentName() != null) {

				if (employees.get(i).getDepartmentName().equals(departmentName)) {
					System.out.println(
							"SSN " + employees.get(i).getSsn() + "Department " + employees.get(i).getDepartmentName());
					suitEmployees.add(employees.get(i));
				}

				else {
					System.out.println("no suit department");
				}
			}
		}

		System.out.println("enter employee's name to search");
		displayEmployeeByClassify();
		String firstName = sc.next();
		for (int j = 0; j < suitEmployees.size(); j++) {
			if (suitEmployees.get(j).getFirstName().equals(firstName)) {
				searchEmployee = suitEmployees.get(j);
			}
		}

		System.out.println(searchEmployee.getSsn() + "First Name " + searchEmployee.getFirstName() + "departemtn name "
				+ searchEmployee.getDepartmentName());

	}

	public static void searchEmyName(String name) {
		Employee em = null;
		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getFirstName().equals(name)) {
				em = employees.get(i);
			}
		}

		System.out.println("SSN " + em.getSsn() + "Department " + em.getDepartmentName());
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int key = 0;
		while (key != 8) {
			System.out.println("1.Input department \r\n" + "2.Display department\r\n" + "3.Input employee \r\n"
					+ "4.Add employee to department\r\n" +

					"5.Display Classify employees\r\n" + "6.Employee Search\r\n" + "7.Report\r\n" + "8.Exit");
			System.out.println("choose task : ");
			key = sc.nextInt();
			switch (key) {
			case 1:
				inputDepartment();
				break;
			case 2:
				displayDepartment();
				break;
			case 3:
				inputEmployee();
				break;
			case 4:
				addEmployeeToDepartment();
				break;
			case 5:
				displayEmployeeByClassify();
				break;
			case 6:
				System.out.println("1.Search by name\r\n" + "2.Search by department");
				String choice = sc.next();
				if ("1".equals(choice)) {
					System.out.println("enter name to search ");
					String name = sc.next();
					searchEmyName(name);
				} else if ("2".equals(choice)) {

					searchEmployeeByDepartment();
				} else {
					System.out.println("no choice suitable");
				}

				break;
			case 7:
				displayEmByDep();
				break;
			case 8:
				System.exit(0);

			default:
				break;
			}
		}

	}

	public static boolean validatePhone(String phone) {
		String ePattern = "^[0-9]{7,}";
		Pattern p = Pattern.compile(ePattern);
		Matcher m = p.matcher(phone);
		return m.matches();

	}

	final static String DATE_FORMAT = "dd-MM-yyyy";

	public static boolean validateBithDate(String date) {
		try {
			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}

	}

	public static boolean validateEmail(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();

	}

}
