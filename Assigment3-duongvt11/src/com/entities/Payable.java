package com.entities;

public interface Payable {
	public double getPaymentAmount();

}
