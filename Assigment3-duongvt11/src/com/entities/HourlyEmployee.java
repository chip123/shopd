package com.entities;

public class HourlyEmployee extends Employee{
	 private double rate;
	 private double workingHours;
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}
	public HourlyEmployee(String ssn, String firstName, String lastName, String birthDate, String phone, String email,
			double rate, double workingHours) {
		super(ssn, firstName, lastName, birthDate, phone, email);
		this.rate = rate;
		this.workingHours = workingHours;
	}
	public HourlyEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public HourlyEmployee(String ssn, String firstName, String lastName, String birthDate, String phone, String email) {
		super(ssn, firstName, lastName, birthDate, phone, email);
		// TODO Auto-generated constructor stub
	}
	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		return 0;
	}
	 
	

}
