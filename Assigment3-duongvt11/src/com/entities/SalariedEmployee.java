package com.entities;

public class SalariedEmployee extends Employee {
	private double commisstionRate;
	private double grossSales;
	private double basicSalary;
	public double getCommisstionRate() {
		return commisstionRate;
	}
	public void setCommisstionRate(double commisstionRate) {
		this.commisstionRate = commisstionRate;
	}
	public double getGrossSales() {
		return grossSales;
	}
	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}
	public double getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}
	public SalariedEmployee(String ssn, String firstName, String lastName, String birthDate, String phone, String email,
			double commisstionRate, double grossSales, double basicSalary) {
		super(ssn, firstName, lastName, birthDate, phone, email);
		this.commisstionRate = commisstionRate;
		this.grossSales = grossSales;
		this.basicSalary = basicSalary;
	}
	public SalariedEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SalariedEmployee(String ssn, String firstName, String lastName, String birthDate, String phone,
			String email) {
		super(ssn, firstName, lastName, birthDate, phone, email);
		// TODO Auto-generated constructor stub
	}
	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
