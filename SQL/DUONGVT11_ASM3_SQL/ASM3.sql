CREATE TABLE dbo.Department(
deptNo INT IDENTITY(1,1)  primary key ,
deptName VARCHAR(200),
note TEXT);

CREATE TABLE dbo.Skill(
skillNo INT IDENTITY(1,1)  primary key ,
SkillName VARCHAR(200),
note TEXT);

CREATE TABLE dbo.Employee (empNo VARCHAR(200)primary key ,
empName VARCHAR(200),
birthDay DATE,
email VARCHAR(200) UNIQUE,
deptNo INT,
CONSTRAINT fk_deptNo FOREIGN KEY (deptNo) REFERENCES Department(deptNo),  
mgrNo VARCHAR (200) NOT NULL DEFAULT 0 ,
startDate DATE,
salary MONEY ,
level INT ,
CONSTRAINT ck_level CHECK (level>=1 AND level<=7),
status INT DEFAULT 0 ,
CONSTRAINT ck_status CHECK(status=1 OR status=2 OR status=0 ),
note TEXT);CREATE TABLE dbo.EmpSkill(
skillNo INT ,
CONSTRAINT fk_skillNo FOREIGN KEY (skillNo) REFERENCES Skill(skillNo) ,
empNo VARCHAR(200),
CONSTRAINT fk_empNo FOREIGN KEY (empNo) REFERENCES Employee(empNo) ,
skillLevel INT 
CONSTRAINT ck_skillLevel CHECK (skillLevel>=1 AND skillLevel<=3),
regDate DATE,
description TEXT,
PRIMARY KEY(skillNo, empNo));

INSERT INTO dbo.Department VALUES('A','A');
INSERT INTO dbo.Department VALUES('B','B');


INSERT INTO dbo.Employee VALUES ('1','DUONG','03/02/2000','',1,'','01/10/2000',12.84,1,1,'');
INSERT INTO dbo.Employee VALUES ('2','DUONGKK','03/02/2000','FF',1,'','01/10/2019',12.84,1,1,'');
INSERT INTO dbo.Employee VALUES ('3','DUONGKKS','03/02/2000','FFD',2,'','01/10/2019',12.84,2,2,'');
INSERT INTO dbo.Employee VALUES ('4','DUONGKKSD','03/02/2000','FFDS',2,'','01/10/2019',12.84,2,0,'');
INSERT INTO dbo.Skill VALUES ('C++','');
INSERT INTO dbo.Skill VALUES ('.NET','');
INSERT INTO dbo.Skill VALUES ('JAVA','');
INSERT INTO dbo.EmpSkill VALUES (1,'1',1,'02/02/2000','');
INSERT INTO dbo.EmpSkill VALUES (2,'1',1,'02/02/2000','');
INSERT INTO dbo.EmpSkill VALUES (3,'2',1,'02/02/2000','');
INSERT INTO dbo.EmpSkill VALUES (2,'2',2,'02/02/2000','');
INSERT INTO dbo.EmpSkill VALUES (2,'3',2,'02/02/2000','');
SELECT * FROM dbo.Employee e WHERE (datediff(month,e.startDate,getdate()))>=6;
SELECT * FROM dbo.Employee e WHERE e.empNo IN (SELECT ek.empNo FROM dbo.EmpSkill ek WHERE ek.skillNo IN 
(SELECT s.skillNo FROM dbo.Skill s WHERE s.SkillName='.NET' OR s.SkillName='C++'));
SELECT * FROM dbo.Department d WHERE d.deptNo IN (SELECT e.deptNo FROM dbo.Employee e  GROUP BY e.deptNo  HAVING COUNT(e.empNo)>1 );
SELECT * FROM dbo.Employee e WHERE e.deptNo IN (SELECT e.deptNo FROM dbo.Employee e  GROUP BY e.deptNo  HAVING COUNT(e.empNo)>1) ORDER BY e.empName DESC;
SELECT * FROM dbo.Employee e WHERE e.status=0 AND e.empNo IN (SELECT ek.empNo FROM dbo.EmpSkill ek GROUP BY ek.empNo HAVING COUNT(ek.empNo)>1);
CREATE VIEW WorkingEmployee AS SELECT * FROM dbo.Employee e WHERE e.status=0;
SELECT * FROM WorkingEmployee;
select datediff(day,'2019-01-16',getdate()) 
select datediff(year,'2018-03-16',getdate()) 
select datediff(month,'2018-11-16',getdate()) 
