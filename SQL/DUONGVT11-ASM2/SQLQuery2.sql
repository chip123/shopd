create table dbo.Trainee (TraineeID int IDENTITY(1,1) PRIMARY KEY not null  ,
FullName varchar(50) ,BirthDate date, Gender varchar(10),ETIQ int , ETGmath int ,
ETEnglish int ,
TrainingClass varchar(10), EvaluationNotes text,
CONSTRAINT CHK_ETIQ CHECK(ETIQ>=0 AND ETIQ<=20),
CONSTRAINT CHK_ETGmath CHECK(ETGmath>=0 AND ETIQ<=20),
CONSTRAINT CHK_ETEnglish CHECK(ETEnglish>=0 and ETEnglish<=50));


alter table dbo.Trainee add  FsoftAccount varchar(10) NOT NULL UNIQUE;

insert into dbo.Trainee values ('','','',12,12,12,'','','');
insert into dbo.Trainee values ('lan','02-10-2000','male',12,12,12,'java','good','4444');
insert into dbo.Trainee values ('lanh','02-11-2000','male',1,1,1,'java','good','44446');
insert into dbo.Trainee values ('lanhj','02-12-2000','male',18,18,18,'java','good','444469');
insert into dbo.Trainee values ('lanhjk','02-12-2000','male',18,18,18,'java','good','4444869');
insert into dbo.Trainee values ('lanhjk','02-11-2001','male',18,18,18,'java','good','4444869');
insert into dbo.Trainee values ('lanhjk','02-11-2000','male',18,18,18,'java','good','44448609');
CREATE VIEW [ViewTrainee] AS SELECT *
FROM dbo.Trainee t
WHERE (t.ETEnglish+t.ETGmath)>=18  ;
select * from [ViewTrainee];


CREATE VIEW view_Trainee AS SELECT *
FROM dbo.Trainee t 
WHERE (t.ETEnglish+t.ETGmath)>=18 and t.ETIQ>=8 and t.ETGmath>=8 and t.ETEnglish >=18 ;

select * from view_Trainee inner join (select vt.BirthDate,MONTH(vt.BirthDate) as month from view_Trainee vt group by vt.BirthDate) as bt on bt.BirthDate=view_Trainee.BirthDate;

SELECT TOP 1 t.FullName,t.BirthDate,  GETDATE() as today,DATEDIFF(yy,t.BirthDate,GETDATE())as age,LEN(t.FullName) AS Lenght FROM Trainee t  ORDER BY LEN(t.FullName) DESC;








