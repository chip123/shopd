create table department(
deptNo int IDENTITY(1,1) PRIMARY KEY,
deptName varchar(10),
note text);

create table skill(skillNo int IDENTITY(1,1) PRIMARY KEY,
skillName varchar(50), 
note text ,
);

create table EMPLOYEE(empNo varchar(10) primary key,
 empName varchar(50) ,
birthDay date,
 deptNo int,
MgrNo varchar(50) not null,
 startDate date,
 salary money,
levelOfTheEmployee int ,
status int,
note text,
CONSTRAINT CHK_PersonlevelOfTheEmployee CHECK (levelOfTheEmployee>=1 AND levelOfTheEmployee<=7),
CONSTRAINT CHK_status CHECK (status=1 or status=0 or status=2));



create table EMP_SKILL(
skillNo int  foreign key REFERENCES skill(skillNo),
 empNo varchar(10) foreign key REFERENCES EMPLOYEE(empNo), 
skillLevel int,
regDate date, 
description text,
CONSTRAINT PK_EMP_SKILL PRIMARY KEY (skillNo,empNo),
CONSTRAINT CHK_skillLevel CHECK (skillLevel>=1 AND skillLevel<=3)
);


alter table employee add email varchar(40);
ALTER TABLE EMPLOYEE
ADD CONSTRAINT df_MgrNo 
DEFAULT '0' FOR MgrNo;
ALTER TABLE EMPLOYEE ADD CONSTRAINT df_status
DEFAULT 0 FOR status;

ALTER TABLE EMPLOYEE ADD 
CONSTRAINT fk_deptNo FOREIGN KEY (deptNo)
    REFERENCES department(deptNo);

	ALTER TABLE EMP_SKILL
DROP COLUMN description;

insert into department values('','');
insert into skill values('','');
insert into EMPLOYEE values('','','',1,'','10-12-1993',99,9,9,'');
create view EMPLOYEE_TRACKING as select  empNo, empName ,levelOfTheEmployee from EMPLOYEE where 
levelOfTheEmployee>=3 and levelOfTheEmployee<=5;




